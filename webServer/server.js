const express = require('express');
const favicon = require('serve-favicon');
const path = require('path');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

const hostname = 'localhost';
const port = 8081;

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.set('view engine', 'ejs');
server.use(favicon(path.join(__dirname, '/public', '/images/favicon.ico')));
server.use(cookieParser());


// front pages router
server.get('/', function(req, res) {
    res.render('../routes/main.ejs', { page: "signin"});
});

server.get('/errorSignIn', function (req, res) {
    res.render('../routes/main.ejs', { page: "errorSignIn" });
});

server.get('/signup', function (req, res) {
    res.render('../routes/main.ejs', { page: "signup" });
});

server.get('/errorSignUp', function (req, res) {
    res.render('../routes/main.ejs', { page: "errorSignUp" });
});

server.get('/userCreate', function (req, res) {
    res.render('../routes/main.ejs', { page: "userCreate" });
});

server.get('/chat', function (req, res) {
    res.render('../routes/main.ejs', { page: "chat" });
});

server.get('/profil', function (req, res) {
    res.render('../routes/main.ejs', { page: "profil" });
});

server.use(express.static(__dirname + '/public'));
server.listen(port, hostname, function() {
    console.log("Server address on http://" + hostname + ":" + port);
});

// SignUp 
server.post('/subscribe', async (req, res) => {
    console.log(req.body);
    let sendReq = await axios.post('http://localhost:5000/CreateUser', req.body);
    console.log(sendReq.data.StatusCode);
    if (sendReq.data.StatusCode === 1000) {
        res.redirect('http://localhost:8081/errorSignUp');
    } else {
        res.redirect('http://localhost:8081/userCreate');
    }    
})

// SignIn
server.post('/login', async (req, res) => {
    console.log(req.body);
    let cookieUser = req.body.user_name;
    console.log(cookieUser);
    let signIn = await axios.post('http://localhost:5000/AuthenticateUser', req.body);
    console.log(signIn.data.StatusCode);
    if (signIn.data.StatusCode === 1000) {
        res.redirect('http://localhost:8081/errorSignIn');
    } else {
        console.log("MSG: " + cookieUser);
        res.cookie("name", cookieUser).redirect('http://localhost:8081/chat');
    }
})

// Add Message
// server.post('/newMessage')